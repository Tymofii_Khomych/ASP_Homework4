﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace task1.Controllers
{
    public class HomeController : Controller
    {   
        public IActionResult Index()
        {
            return View();
        }
    }
}